from rest_framework.permissions import BasePermission


class IsKioskOrReject(BasePermission):
    def has_permission(self, request, view):
        """
        Returns if the kiosk is authenticated or not.
        """
        return request.kiosk is not None


class KioskHasLoggedInEmployee(BasePermission):
    def has_permission(self, request, view):
        """
        Returns if the authenticated kiosk has an logged in employee
        """
        if request.kiosk is None:
            return False
        else:
            return request.kiosk.logged_in_employee is not None
