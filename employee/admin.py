from django.contrib import admin

from employee.models import Employee


class EmployeeAdmin(admin.ModelAdmin):
    def get_object(self, *args, **kwargs):
        object = super(EmployeeAdmin, self).get_object(*args, **kwargs)

        # Hides the pin from the admin.
        object.pin = ""

        return object


# Registers the kiosk model with the admin site.
admin.site.register(Employee, EmployeeAdmin)
