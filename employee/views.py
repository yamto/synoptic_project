# Create your views here.
from django.http import Http404
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.routers import DefaultRouter

from employee.models import Employee
from employee.permissions import IsKioskOrReject, KioskHasLoggedInEmployee
from employee.serializers import EmployeeSerializer, EmployeeLoginSerializer, EmployeePaySerializer, \
    EmployeeDepositSerializer, EmployeeLogoutSerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    """
    Handles api requests relating to the employee api endpoint.
    """

    queryset = Employee.objects.all()
    permission_classes = (IsKioskOrReject, )

    serializer_class = EmployeeSerializer

    lookup_field = 'employee_id'

    def get_object(self):

        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )

        lookup_value = self.kwargs[lookup_url_kwarg]

        if lookup_value == 'current':
            employee = self.request.kiosk.logged_in_employee
            if employee:
                return employee
            else:
                raise Http404
        return super(EmployeeViewSet, self).get_object()

    @action(detail=False, name="Login/Act as Employee", serializer_class=EmployeeLoginSerializer, methods=['post'])
    def login(self, request):
        """
        Handles the employee login action on the employee viewset.
        """

        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():

            serializer.login(self.request.kiosk)

            return Response(
                EmployeeSerializer(instance=serializer._employee).data
            )
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, name='Deposit Credit', url_path="current/deposit", serializer_class=EmployeeDepositSerializer,
            methods=['post'], permission_classes=(KioskHasLoggedInEmployee, IsKioskOrReject))
    def deposit(self, request):
        """
        Handles the employee deposti action on the employee viewset.
        """

        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():

            serializer.deposit()

            return Response(
                EmployeeSerializer(instance=request.kiosk.logged_in_employee).data
            )
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, url_path="current/pay", serializer_class=EmployeePaySerializer, methods=['post'],
            permission_classes=(KioskHasLoggedInEmployee, IsKioskOrReject))
    def pay(self, request):
        """
        Handles the employee pay action on the employee viewset.
        """

        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():

            serializer.pay()

            return Response(
                EmployeeSerializer(instance=request.kiosk.logged_in_employee).data
            )
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, name="Logout Employee from kiosk", serializer_class=EmployeeLogoutSerializer, methods=['post'], permission_classes=(IsKioskOrReject, ))
    def logout(self, request):
        """
        Handles the employee login action on the employee viewset.
        """

        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():

            serializer.logout(self.request.kiosk)

            return Response(
                {
                    "employee": None,
                    "message": "You've been logged out."
                },
                status=status.HTTP_204_NO_CONTENT
            )
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


employee_router = DefaultRouter()
employee_router.register('employees', EmployeeViewSet, base_name='employees')



