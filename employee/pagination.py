from rest_framework.pagination import PageNumberPagination


class StandardResultPagination(PageNumberPagination):
    page_size = 20
    max_page_size = 50
