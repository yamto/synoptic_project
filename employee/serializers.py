from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from employee.exceptions import InsufficientFunds
from employee.models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    """
    Handles parsing and serializing of employee instances.
    """

    pin = serializers.IntegerField(max_value=9999, write_only=True)

    balance = serializers.SerializerMethodField()

    class Meta:
        model = Employee

        fields = [
            'card_id',
            'employee_id',
            'name',
            'email',
            'mobile_number',
            'balance',
            'pin'
        ]

    def get_balance(self, obj):
        return obj.balance

class EmployeeLoginSerializer(serializers.Serializer):
    """
    Handles parsing and serializing of login data.
    """

    card_id = serializers.CharField(max_length=16)
    pin = serializers.CharField(max_length=4)

    def __init__(self, *args, **kwargs):
        """
        Initialises additional attributes of the serializer.
        """
        super(EmployeeLoginSerializer, self).__init__(*args ,**kwargs)

        self._employee = None

    def validate_card_id(self, card_id):
        """
        Checks that the card_id has an associated employee (is valid)
        """

        try:
            self._employee = Employee.objects.get(card_id=card_id)
        except ObjectDoesNotExist:
            raise ValidationError("This card id is not registered.")

        return card_id

    def validate(self, attrs):
        """
        Calls validate pin in validate method so we can make sure
        :param attrs:
        :return:
        """
        validated_data = super(EmployeeLoginSerializer, self).validate(attrs)

        self.validate_pin_is_valid(validated_data['pin'])
        return validated_data

    def validate_pin_is_valid(self, pin):
        """
        Checks the pin is valid otherwise raises an error
        """

        if self._employee is None:
            raise ValidationError({"pin": ["Incorrect pin please try again."]})

        is_valid = self._employee.is_pin_valid(pin)

        if not is_valid:
            raise ValidationError({"pin": ["Incorrect pin please try again."]})
        return pin

    def login(self, kiosk):
        """
        Login the employee into the kiosk
        """

        if self._employee is None:
            raise ValueError("Employee should be set before calling this method.")

        pin = self.validated_data.get('pin')

        logged_in = kiosk.cache.login_employee(self._employee, pin)

        return logged_in


class EmployeeLogoutSerializer(serializers.Serializer):
    """
    Parses and serializes data between outside and inside of the application
    """

    def validate(self, attrs):
        """
        Validates that kiosk is logged and has a authenticated employee.
        """

        validated_data = super(EmployeeLogoutSerializer, self).validate(attrs)

        kiosk = self.context['request'].kiosk

        assert kiosk, "A kiosk must be logged in to use this serializer."

        if not kiosk.logged_in_employee:
            raise ValidationError("There is not employee currently logged in.")
        return validated_data

    def logout(self, kiosk):
        """
        Logs the employee out of the kiosk.
        """

        assert hasattr(self, '_errors'), (
            'You must call `.is_valid()` before calling `.logout()`.'
        )

        return kiosk.cache.logout_employee()


class AbstractPaymentSerializer(serializers.Serializer):
    """
    Abstraction class for managing amounts and setting the employee
    """

    amount = serializers.DecimalField(decimal_places=2, max_digits=10)

    def __init__(self, *args, **kwargs):
        super(AbstractPaymentSerializer, self).__init__(*args, **kwargs)
        self._employee = self.context['request'].kiosk.logged_in_employee


class EmployeePaySerializer(AbstractPaymentSerializer):
    """
    Handles parsing and serializing of payment data.

    """
    def validate_amount(self, amount):
        """
        Validates the employee has sufficient funds to pay.
        """

        try:
            self._employee.can_pay_amount(amount, raise_exception=True)
            return amount
        except InsufficientFunds:
            raise ValidationError(
                "This account have insufficient funds to complete this transaction"
            )

    def pay(self):
        """
        Deducts the amount of money from the employee.
        """

        amount = self.validated_data['amount']
        self._employee.pay(amount, save=True)


class EmployeeDepositSerializer(AbstractPaymentSerializer):
    """
    Handles parsing and serializing of depositing money data.
    """
    def deposit(self):
        """
        Deposit the amount of money to the employee.
        """
        amount = self.validated_data['amount']

        self._employee.deposit(amount, save=True)
