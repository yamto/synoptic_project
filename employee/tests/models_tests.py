from random import randint

import pytest
from django.db import IntegrityError
from django.test import TestCase

from employee.exceptions import InsufficientFunds, InvalidPin
from employee.models import Employee
from employee.tests.factories import EmployeeFactory


class TestsEmployee(TestCase):
    """
    Test suite which tests the Employee model
    """
    def test_create(self):
        current_employee_count = Employee.objects.count()
        first_employee = EmployeeFactory()

        self.assertEqual(
            current_employee_count + 1,
            Employee.objects.count(),
            "Should have save a new instance in the database."
        )

        with self.subTest("Card id is unique"):
            with pytest.raises(IntegrityError):
                EmployeeFactory(card_id=first_employee.card_id)

    def test_str(self):

        employee = EmployeeFactory()

        self.assertEqual(
            employee.__str__(),
            employee.employee_id,
            "Should return the employee's id number."
        )

    def test_deposit(self):

        employee = EmployeeFactory.build(balance=300)

        employee.deposit(1000.10)

        self.assertEqual(
            employee.balance,
            1300.10,
            "Should increase the amount balance by 1000.10"
        )
        self.assertFalse(employee.id, "Should not have saved the instance.")

        employee.deposit(1000.10, save=True)

        self.assertTrue(employee.id, "Should save the employee instance.")

    def test_can_pay_amount(self):
        employee = EmployeeFactory.build(balance=200)

        self.assertTrue(
            employee.can_pay_amount(100),
            "Employee should be able to pay the amount."
        )

        self.assertTrue(
            employee.can_pay_amount(200),
            "Employee should be able to pay the amount."
        )

        self.assertFalse(
            employee.can_pay_amount(300),
            "Employee should not be able to pay the required amount."
        )

        # Should not raise error as employee has sufficient funds.
        employee.can_pay_amount(200, raise_exception=True)

        # Should raise error if insufficient funds
        with pytest.raises(InsufficientFunds):
            employee.can_pay_amount(300, raise_exception=True)

    def test_pay(self):
        employee = EmployeeFactory.build(balance=200)

        current_balance = employee.balance

        self.assertEqual(
            employee.pay(50),
            current_balance - 50,
            "Should deduct the money from the balance"
        )
        self.assertFalse(employee.id, "Should not have saved the instance.")

        current_balance = employee.balance

        self.assertEqual(employee.pay(current_balance), 0, "Should deduct the money from the balance")

        # Should raise error as employee can't pay.
        with pytest.raises(InsufficientFunds):
            employee.pay(300),

        employee.balance = 100

        employee.pay(100, save=True)

        self.assertTrue(employee.id, "Should save the employee instance.")

    def test_is_pin_valid(self):

        pin = randint(0, 2000)

        employee = EmployeeFactory()

        employee.set_pin(pin)

        with self.subTest("With valid pin"):
            self.assertTrue(employee.is_pin_valid(pin), "Should return for the correct pin")

            # Should not raise a exception
            employee.is_pin_valid(pin, raise_exception=True)

        with self.subTest("with invalid pin"):
            self.assertFalse(employee.is_pin_valid(1321312), "Should return false for an incorrect pin.")

            with pytest.raises(InvalidPin):
                employee.is_pin_valid(1321312, raise_exception=True)

    def test_set_pin(self):

        employee = EmployeeFactory.build()

        pin = 31312

        employee.set_pin(pin)

        self.assertNotEqual(pin, employee.pin, "Should not store the pin in plain text format.")

        self.assertTrue('pbkdf2_sha256$120000$' in employee.pin, "Should return hashed version of the pin.")

    def test_save(self):

        pin = 1231

        employee = EmployeeFactory.build()
        employee.pin = pin

        employee.save()

        self.assertTrue('pbkdf2_sha256$120000$' in employee.pin, "The pin value should be hashed on save.")

