import pytest
from django.core.cache import cache
from django.http import Http404
from django.test import TestCase, RequestFactory
from rest_framework.reverse import reverse

from employee.tests.factories import EmployeeFactory
from employee.views import EmployeeViewSet
from kiosk.interfaces import KioskCacheInterface
from kiosk.tests.factories import KioskFactory


class TestsViewSet(TestCase):

    viewset_class = EmployeeViewSet

    def test_get_object_with_current_in_lookup(self):

        cache.clear()

        viewset = self.viewset_class()
        viewset.kwargs = {
            viewset.lookup_field: "current"
        }

        employee = EmployeeFactory()

        viewset.request = RequestFactory().get('/')
        viewset.request.kiosk = KioskFactory()

        with self.subTest("With no logged in employee"):
            # Should raise http404 as no employee is logged in.
            with pytest.raises(Http404):
                viewset.get_object()

        with self.subTest("With logged in employee"):
            viewset.request.kiosk.cache._login_employee(employee)
            self.assertEqual(viewset.get_object(), employee, "Should return the currently logged in employee.")

        with self.subTest("With the pk in the lookup field"):
            viewset.kwargs = {
                viewset.lookup_field: getattr(employee, viewset.lookup_field)
            }

            self.assertEqual(viewset.get_object(), employee, "Should return the employee related to lookup field.")

        with self.subTest("With an invalid pk in the lookup field"):
            viewset.kwargs = {
                viewset.lookup_field: "a invalid pk"
            }

            # Should raise http404 as no employee is associated with the value of the lookup field
            with pytest.raises(Http404):
                viewset.get_object()


class AuthenticationViewSetMixin:

    url = None

    def setUp(self):
        self.kiosk = KioskFactory()
        self.authenticated_url = f'{self.url}?jwt={self.kiosk.get_authentication_token()}'


class TestsEmployeeLoginView(AuthenticationViewSetMixin, TestCase):
    url = reverse('employees-login')

    authenticated_url = None

    def test_get(self):
        with self.subTest("Unauthenticated kiosk"):
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 403, "Should return 403 permission denied")

        with self.subTest("Authenticated kiosk"):
            response = self.client.get(self.authenticated_url)
            self.assertEqual(response.status_code, 405, "Should return method not allowed for GETS")

    def test_post_unauthenticated(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, 403, "Should return 403 permission denied")

    def test_post_authenticated(self):
        test_cases = (
            'card_id',
            'pin'
        )

        pin = 1234

        employee = EmployeeFactory(pin=pin)

        data = {
            "card_id": employee.card_id,
            "pin": pin
        }

        from django.core.cache import cache

        cache.clear()

        for field in test_cases:
            with self.subTest(f"POSTing without {field}"):
                post_data = data.copy()
                post_data.pop(field)
                print(post_data)

                response = self.client.post(self.authenticated_url, data=post_data)

                self.assertEqual(response.status_code, 400, "Should return 400 bad request")
                self.assertEqual(
                    self.kiosk.logged_in_employee, None, "Should return none as no employee should be logged in."
                )

        with self.subTest("POSTING with correct_data"):
            response = self.client.post(self.authenticated_url, data=data)
            self.assertEqual(response.status_code, 200, "Should return 200 (OK)")

            self.assertEqual(self.kiosk.logged_in_employee, employee, "Should return the logged in employee.")


class TestsEmployeeDepositView(AuthenticationViewSetMixin, TestCase):

    url = reverse('employees-deposit')

    def test_get(self):
        with self.subTest("Unauthenticated kiosk"):
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 403, "Should return 403 permission denied")

        with self.subTest("Authenticated kiosk"):
            response = self.client.get(self.authenticated_url)
            self.assertEqual(
                response.status_code, 403, "Should return 403 permission denied as employee is not logged in"
            )

        with self.subTest("Authenticated kiosk & logged in employee"):
            employee = EmployeeFactory()

            self.kiosk.cache._login_employee(employee)

            response = self.client.get(self.authenticated_url)
            self.assertEqual(response.status_code, 405, "Should return method not allowed for GETS")

    def test_post_unauthenticated(self):
        with self.subTest("Unauthenticated kiosk"):
            response = self.client.post(self.url)
            self.assertEqual(response.status_code, 403, "Should return 403 permission denied")

        with self.subTest("Authenticated kiosk but not logged in employee"):
            response = self.client.post(self.authenticated_url)
            self.assertEqual(
                response.status_code, 403, "Should return 403 permission denied as employee is not logged in"
            )

    def test_post_authenticated(self):
        amount = 200

        data = {
            "amount": amount
        }

        cache.clear()

        self.kiosk.cache._login_employee(EmployeeFactory())

        employee = self.kiosk.logged_in_employee
        current_balance = employee.balance

        with self.subTest("POSTing without amount"):

            response = self.client.post(self.authenticated_url, data={})

            self.assertEqual(response.status_code, 400, "Should return 400 bad request")

            employee.refresh_from_db()

            self.assertEqual(current_balance, employee.balance, "The balance should not have changed.")

        with self.subTest("POSTING with correct_data"):
            response = self.client.post(self.authenticated_url, data=data)
            self.assertEqual(response.status_code, 200, "Should return 200 (OK)")

            employee.refresh_from_db()

            self.assertEqual(self.kiosk.logged_in_employee.balance, current_balance + amount,
                             "Should deposit the amount to the employee's balance")


class TestsEmployeePayView(AuthenticationViewSetMixin, TestCase):

    url = reverse("employees-pay")

    def test_get(self):
        with self.subTest("Unauthenticated kiosk"):
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 403, "Should return 403 permission denied")

        with self.subTest("Authenticated kiosk"):
            response = self.client.get(self.authenticated_url)
            self.assertEqual(
                response.status_code, 403, "Should return 403 permission denied as employee is not logged in"
            )

        with self.subTest("Authenticated kiosk & logged in employee"):
            employee = EmployeeFactory()

            self.kiosk.cache._login_employee(employee)

            response = self.client.get(self.authenticated_url)
            self.assertEqual(response.status_code, 405, "Should return method not allowed for GETS")

    def test_post_unauthenticated(self):
        with self.subTest("Unauthenticated kiosk"):
            response = self.client.post(self.url)
            self.assertEqual(response.status_code, 403, "Should return 403 permission denied")

        with self.subTest("Authenticated kiosk but not logged in employee"):
            response = self.client.post(self.authenticated_url)
            self.assertEqual(
                response.status_code, 403, "Should return 403 permission denied as employee is not logged in"
            )

    def test_post_authenticated(self):
        amount = 200

        data = {
            "amount": amount
        }

        cache.clear()

        self.kiosk.cache._login_employee(EmployeeFactory(balance=200))

        employee = self.kiosk.logged_in_employee
        current_balance = employee.balance

        with self.subTest("POSTing without amount"):

            response = self.client.post(self.authenticated_url, data={})

            self.assertEqual(response.status_code, 400, "Should return 400 bad request")

            employee.refresh_from_db()

            self.assertEqual(current_balance, employee.balance, "The balance should not have changed.")
            self.assertEqual(response.data['amount'][0], "This field is required.", "Should return the correct error.")

        current_balance = employee.balance

        with self.subTest("POSTING with correct_data"):
            response = self.client.post(self.authenticated_url, data=data)
            self.assertEqual(response.status_code, 200, "Should return 200 (OK)")

            employee.refresh_from_db()

            self.assertEqual(self.kiosk.logged_in_employee.balance, current_balance - amount,
                             "Should pay the amount out of the employee's balance")

        current_balance = employee.balance

        with self.subTest("POSTING with correct_data but with insufficient funds"):
            response = self.client.post(self.authenticated_url, data=data)
            self.assertEqual(response.status_code, 400, "Should return 400 due to insufficient funds.")

            self.assertEqual(
                response.data['amount'][0], "This account have insufficient funds to complete this transaction",
                "Should return the correct error"
            )

            employee.refresh_from_db()

            self.assertEqual(current_balance, employee.balance, "The balance should not have changed.")


class TestsEmployeeLogoutView(AuthenticationViewSetMixin, TestCase):
    url = reverse("employees-logout")

    def test_get(self):
        with self.subTest("Unauthenticated kiosk"):
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 403, "Should return 403 permission denied")

        with self.subTest("Authenticated kiosk"):
            response = self.client.get(self.authenticated_url)
            self.assertEqual(
                response.status_code, 405, "Should return method not allowed for GETS"
            )

    def test_post_unauthenticated(self):
        with self.subTest("Unauthenticated kiosk"):
            response = self.client.post(self.url)
            self.assertEqual(response.status_code, 403, "Should return 403 permission denied")

        with self.subTest("Authenticated kiosk but not logged in employee"):
            response = self.client.post(self.authenticated_url)
            self.assertEqual(
                response.status_code, 400, "Should return 400 as no employee is logged in to logout."
            )
            self.assertEqual(
                response.data['non_field_errors'][0], "There is not employee currently logged in.",
                "Should return the correct error."
            )

    def test_post_authenticated(self):

        cache.clear()

        self.kiosk.cache._login_employee(EmployeeFactory())

        response = self.client.post(self.authenticated_url, data={})

        logged_in_employee = KioskCacheInterface(jwt=self.kiosk.get_authentication_token()).logged_in_employee

        self.assertIsNone(logged_in_employee, "Should be None as the employee should be logged out.")

        self.assertEqual(response.status_code, 204, "Should return 204 as the user is logged out.")
        self.assertEqual(
            response.data,  {"employee": None, "message": "You've been logged out."}, "Should return correct response."
        )
