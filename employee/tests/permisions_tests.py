from django.test import TestCase, RequestFactory

from employee.permissions import IsKioskOrReject, KioskHasLoggedInEmployee
from employee.tests.factories import EmployeeFactory
from kiosk.tests.factories import KioskFactory


class TestsIsKioskOrReject(TestCase):

    permission_class = IsKioskOrReject

    def test_has_permission(self):

        request = RequestFactory().get('/')
        permission = self.permission_class()

        with self.subTest("With no logged in kiosk"):

            request.kiosk = None

            self.assertFalse(permission.has_permission(request, False), "Should return false as no kiosk is logged in.")

        with self.subTest("With logged in kiosk"):

            request.kiosk = KioskFactory()

            self.assertTrue(permission.has_permission(request, False), "Should return true because of logged in kiosk.")


class TestsKioskHasLoggedInEmployee(TestCase):

    permission_class = KioskHasLoggedInEmployee

    def test_has_permission(self):
        request = RequestFactory().get('/')
        permission = self.permission_class()

        with self.subTest("With no logged in kiosk"):

            request.kiosk = None

            self.assertFalse(permission.has_permission(request, False), "Should return false as no kiosk is logged in.")

        request.kiosk = KioskFactory()

        with self.subTest("With no logged in employee"):
            self.assertFalse(
                permission.has_permission(request, False), "Should return false as the employee is not logged in"
            )

        with self.subTest("with logged in employee"):

            request.kiosk.cache._login_employee(EmployeeFactory())

            self.assertTrue(
                permission.has_permission(request, False), "Should return true because employee is logged in."
            )

