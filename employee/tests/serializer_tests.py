import pytest
from django.core.cache import cache
from django.test import TestCase, RequestFactory
from rest_framework.exceptions import ValidationError

from employee.serializers import EmployeeLoginSerializer, AbstractPaymentSerializer, EmployeePaySerializer, \
    EmployeeDepositSerializer, EmployeeLogoutSerializer
from employee.tests.factories import EmployeeFactory
from kiosk.tests.factories import KioskFactory


class TestsEmployeeLoginSerializer(TestCase):

    serializer = EmployeeLoginSerializer

    def test_init(self):

        serializer = self.serializer()

        self.assertEqual(serializer._employee, None, "Should initialise the ._employee attribute.")

    def test_validate_card_id(self):
        serializer = self.serializer()

        employee = EmployeeFactory()

        with self.subTest("Employee Doesn't Exist"):
            # Should raise a validation error
            with pytest.raises(ValidationError):
                serializer.validate_card_id("a fake card id")

            self.assertEqual(serializer._employee, None, "Should not set the employee as the card id id fake.")

        with self.subTest("Employee exists"):
            returned = serializer.validate_card_id(employee.card_id)

            self.assertEqual(returned, employee.card_id, "Should return the valid card id")
            self.assertEqual(serializer._employee, employee, "Should set the employee to that of the card id.")

    def test_validate_pin(self):
        serializer = self.serializer()

        pin = 1234

        with self.subTest("With _employee not being set"):
            # Should raise validation error as the employee isn't set.
            with pytest.raises(ValidationError):
                serializer.validate_pin_is_valid(1231)

        serializer._employee = EmployeeFactory(pin=pin)

        with self.subTest("With a valid pin"):
            returned = serializer.validate_pin_is_valid(pin)

            self.assertEqual(returned, pin, "Should return the validated pin")

        with self.subTest("With invalid pin"):

            # Should raise validation error as is the pin is invalid
            with pytest.raises(ValidationError):
                serializer.validate_pin_is_valid(1231)

    def test_login(self):
        serializer = self.serializer()

        pin = 1234

        employee = EmployeeFactory(pin=pin)

        kiosk = KioskFactory()

        serializer._validated_data = {"pin": pin}

        with self.subTest("Employee value hasn't been set"):
            # The employee attribute should be set before calling the login method.
            with pytest.raises(ValueError):
                serializer.login(kiosk)

        with self.subTest("Employee value has been set"):
            cache.clear()
            serializer._employee = employee
            serializer.login(kiosk)
            self.assertTrue(serializer.login(kiosk), "Should return true as the login was successful")

            self.assertEqual(employee, kiosk.cache.logged_in_employee, "Should login the employee into the kiosk.")


class TestsAbstractPaymentSerializer(TestCase):

    serializer = AbstractPaymentSerializer



    def test_init(self):

        request = RequestFactory().get('/')
        request.kiosk = KioskFactory()

        interface = request.kiosk.cache

        pin = 1234
        employee = EmployeeFactory(pin=1234)

        interface.login_employee(employee, pin)

        serializer = self.serializer(context={
            "request": request
        })

        self.assertEqual(serializer._employee, employee, "Should set the logged in employee of the kiosk.")


class AbstractPaymenttest:

    serializer_class = None

    def test_inherit_AbstractPaymentSerializer(self):
        self.assertTrue(
            issubclass(self.serializer_class, AbstractPaymentSerializer), "Should inherit the abstract class."
        )


class TestsEmployeePaySerializer(AbstractPaymenttest, TestCase):

    serializer_class = EmployeePaySerializer

    def test_validate_amount(self):
        pin = 1234
        employee = EmployeeFactory(balance=200, pin=pin)

        kiosk = KioskFactory()
        interface = kiosk.cache
        interface.login_employee(employee, pin)

        request = RequestFactory().get('/')
        request.kiosk = kiosk

        serializer = self.serializer_class(context={"request": request})

        with self.subTest("With insufficient funds"):
            # Should raise a validation error
            with pytest.raises(ValidationError):
                serializer.validate_amount(300)

        with self.subTest("With sufficient funds"):
            amount = 200
            returned = serializer.validate_amount(amount)
            self.assertEqual(returned, amount, "Should return the amount provided.")

    def test_pay(self):
        pin = 1234
        employee = EmployeeFactory(balance=200, pin=pin)

        kiosk = KioskFactory()
        interface = kiosk.cache
        interface.login_employee(employee, pin)

        request = RequestFactory().get('/')
        request.kiosk = kiosk

        serializer = self.serializer_class(context={"request": request})

        serializer._validated_data = {
            "amount": 200
        }

        current_balance = employee.balance

        serializer.pay()

        # We want to make sure it is actually saved in the database.
        employee.refresh_from_db()

        self.assertEqual(employee.balance, current_balance - 200, "Should remove 200 from the balance.")


class TestsEmployeeDepositSerializer(AbstractPaymenttest, TestCase):

    serializer_class = EmployeeDepositSerializer

    def test_deposit(self):
        pin = 1234
        employee = EmployeeFactory(balance=200, pin=pin)

        kiosk = KioskFactory()
        interface = kiosk.cache
        interface.login_employee(employee, pin)

        request = RequestFactory().get('/')
        request.kiosk = kiosk

        serializer = self.serializer_class(context={"request": request})

        serializer._validated_data = {
            "amount": 200
        }

        current_balance = employee.balance

        serializer.deposit()

        # We want to make sure it is actually saved in the database.
        employee.refresh_from_db()

        self.assertEqual(employee.balance, current_balance + 200, "Should remove 200 from the balance.")


class TestsEmployeeLogoutSerializer(TestCase):

    serializer_class = EmployeeLogoutSerializer

    def test_validate(self):

        request = RequestFactory().get('/')
        request.kiosk = None

        context = {
            "request": request
        }

        with self.subTest("Unauthenticated kiosk"):

            serializer = self.serializer_class(context=context)

            with pytest.raises(AssertionError, match="A kiosk must be logged in to use this serializer."):
                serializer.validate({})

        request = RequestFactory().get('/')
        request.kiosk = KioskFactory()
        context = {
            "request": request
        }

        with self.subTest("Authenticated kiosk but no logged in employee"):
            serializer = self.serializer_class(context=context)

            with pytest.raises(ValidationError):
                serializer.validate({})

        request.kiosk.cache._login_employee(EmployeeFactory())

        with self.subTest("Authenticated kioks and logged in employee"):
            serializer = self.serializer_class(context=context)
            serializer.validate({})
