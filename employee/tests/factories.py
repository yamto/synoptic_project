import random

from factory import django, Faker
from factory.fuzzy import BaseFuzzyAttribute, FuzzyInteger, FuzzyText

from employee.models import Employee


def convert_int_to_pin(pin):

    pin = str(pin)

    for i in range(4 - len(pin)):
        pin = f'0{pin}'

    return pin


class FuzzyPin(BaseFuzzyAttribute):
    """
    Returns a random pin as string
    """

    def __init__(self, **kwargs):
        super(FuzzyPin, self).__init__(**kwargs)

    def fuzz(self):
        return convert_int_to_pin(random.randint(0, 9999))


class EmployeeFactory(django.DjangoModelFactory):
    """
    Creates mock of the employee model for testing.
    """

    class Meta:
        model = Employee

    employee_id = FuzzyInteger(0, 9999)
    card_id = FuzzyText(length=16)

    name = Faker('name')
    email = Faker('email')
    mobile_number = Faker('phone_number')
    balance = 0

    pin = FuzzyPin()
