from django.contrib.auth.hashers import check_password, make_password
from django.core.validators import RegexValidator
from django.db import models

# Create your models here.
from employee.exceptions import InsufficientFunds, InvalidPin


class Employee(models.Model):
    """
    Handles interacts between the application and the database via the ORM
    """

    # We don't use this as the Private key for optimisation reasons.
    employee_id = models.CharField(max_length=64, db_index=True, unique=True)
    card_id = models.CharField(max_length=16, unique=True)

    name = models.CharField(max_length=128)
    email = models.EmailField(unique=True)

    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."
    )
    mobile_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)

    balance = models.DecimalField(decimal_places=2, max_digits=10, default=0)

    pin = models.CharField(max_length=128)

    def __init__(self, *args, **kwargs):

        super(Employee, self).__init__(*args, **kwargs)
        self._initial_pin = self.pin

    def save(self, *args, **kwargs):
        """
        Overrides the default save method to save the pin in hashed format.
        """

        # If hasn't been saved then the pk is none.
        pin_has_changed_or_new = self._initial_pin != self.pin or self.pk is None

        if pin_has_changed_or_new:
            self.set_pin(self.pin)

        super(Employee, self).save(*args, **kwargs)

    def set_pin(self, pin):
        """
        Sets the pin to hashed version (like how the passwords are done).
        """
        self.pin = make_password(pin)

    def __str__(self):
        """
        Returns the String value of the model (the employee id)
        """
        return self.employee_id

    def can_pay_amount(self, amount, raise_exception=False):
        """
        Returns if the instance pay an amount.
        """

        can_pay = self.balance >= amount

        if not can_pay and raise_exception:
            raise InsufficientFunds

        return can_pay

    def deposit(self, amount, save=False):
        """
        Updates the balance of instance with a new amount.
        """
        self.balance += amount

        if save:
            self.save()

        return self.balance

    def pay(self, amount, save=False):
        """
        Throws an exception if the instance can't pay otherwise updates the balance of the instance
        """

        self.can_pay_amount(amount, raise_exception=True)

        self.balance -= amount

        if save:
            self.save()
        return self.balance

    def is_pin_valid(self, pin, raise_exception=False):
        """
        Validates the provided pin against the instance's pin.
        """

        valid = check_password(pin, self.pin)

        if not valid and raise_exception:
            raise InvalidPin

        return valid
