

class InsufficientFunds(Exception):
    pass


class InvalidPin(Exception):
    pass
