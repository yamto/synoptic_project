# **_Membership System_**

###### **Installation**

You will need a Docker-Compose to run this for production

https://www.docker.com/get-started

Once you have docker installed; in the commandline run:
`docker-compose -f docker-compose-production.yml up -d` 

For my information see the attached user documentation in the root directory.

