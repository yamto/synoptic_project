import jwt
import pytest
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.test import TestCase

from kiosk.interfaces import KioskCacheInterface
from kiosk.models import Kiosk
from kiosk.tests.factories import KioskFactory


class TestsKioskManager(TestCase):
    def test_get_for_jwt(self):
        kiosk = KioskFactory()

        json_web_token = kiosk.get_authentication_token()

        with self.subTest("With valid jwt token"):
            returned_kiosk = Kiosk.objects.get_for_jwt(json_web_token)
            self.assertEqual(kiosk, returned_kiosk, "Should return the correct kiosk for the jwt")

        with self.subTest("with not valid jwt token"):
            with pytest.raises(ObjectDoesNotExist):
                Kiosk.objects.get_for_jwt('not a real jwt token')
            with pytest.raises(ObjectDoesNotExist):
                Kiosk.objects.get_for_jwt('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c')


class TestsKiosk(TestCase):
    def test_create(self):

        current_kiosk_amount = Kiosk.objects.count()
        KioskFactory()

        self.assertEqual(
            current_kiosk_amount + 1,
            Kiosk.objects.count(),
            "Should have created a new Kiosk instance."
        )

    def test_str(self):

        kiosk = KioskFactory.build()

        self.assertEqual(kiosk.__str__(), kiosk.name, "Should return the human name of the kiosk")

    def test_get_authentication_token(self):

        kiosk = KioskFactory.build()

        with self.subTest("Kiosk has not been saved"):
            # Should raise a error as the kiosk hasn't been saved.
            with pytest.raises(IntegrityError):
                kiosk.get_authentication_token()

        with self.subTest("Kiosk has been saved"):
            kiosk.save()
            json_web_token = kiosk.get_authentication_token()

            decoded_data = jwt.decode(json_web_token, settings.SECRET_KEY, algorithm='HS256')

            self.assertEqual(
                decoded_data['pk'], kiosk.pk, "Should return the pk associate with the kiosk"
            )

    def test_cache(self):
        kiosk = KioskFactory.build()

        with self.subTest("Unsaved Kiosk instance"):
            # Should throw error if kiosk isn't saved
            with pytest.raises(IntegrityError):
                a = kiosk.cache

        kiosk.pk = 1

        with self.subTest("With saved kiosk instance(with pk)"):
            kiosk_cache_interface = kiosk.cache

            self.assertEqual(
                type(kiosk_cache_interface), KioskCacheInterface, "Should return kiosk cache interface instance"
            )

            self.assertEqual(
                kiosk_cache_interface.jwt, kiosk.get_authentication_token(),
                "Should return the jwt for the kiosk instance"
            )

            self.assertEqual(
                kiosk._cache_interface, kiosk_cache_interface, "Should store the cache interface on the instance"
            )

            kiosk._cache_interface = 'a test'

            self.assertEqual(
                kiosk.cache, 'a test', "Should return the value of ._cache_interface."
            )
