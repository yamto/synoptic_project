import pytest
from django.core.cache import cache
from django.test import TestCase

from employee.exceptions import InvalidPin
from employee.models import Employee
from employee.tests.factories import EmployeeFactory
from kiosk.interfaces import KioskCacheInterface
from kiosk.tests.factories import KioskFactory


class TestsKioskCacheInterface(TestCase):

    interface = KioskCacheInterface

    def test_init(self):
        kiosk = KioskFactory()

        jwt = kiosk.get_authentication_token()
        interface = self.interface(jwt)

        self.assertEqual(interface.jwt, jwt, "Should set the jwt value on the interface.")
        self.assertEqual(interface._employee, None, "Should have a _employee attribute")
        self.assertEqual(interface._employee_pk, None, "Should have a _employee_pk attribute")

    def test_employee_cache_key(self):
        kiosk = KioskFactory.build(pk=1)

        jwt = kiosk.get_authentication_token()

        interface = self.interface(jwt)

        self.assertEqual(interface.employee_cache_key, f'{jwt}-logged_in', "Should return the correct format.")

    def test_logged_in_employee(self):
        pin = 1230
        employee = EmployeeFactory(pin=pin)

        kiosk = KioskFactory()

        jwt = kiosk.get_authentication_token()

        interface = self.interface(jwt)

        cache.clear()

        with self.subTest("With no employee logged in"):
            returned_employee = interface.logged_in_employee

            self.assertIsNone(returned_employee, "Should return none as no employee is logged in.")

        with self.subTest("Logged in employee"):
            interface.login_employee(employee, pin)

            returned_employee = interface.logged_in_employee

            self.assertEqual(type(returned_employee), Employee, "Should return an employee instance.")
            self.assertEqual(returned_employee, employee, "Should return the correct employee")
            self.assertEqual(interface._employee, employee, "Should cache employee on the instance.")

    def test_login_employee(self):
        # We clear the cache to stop interference.
        cache.clear()
        pin = 1230
        employee = EmployeeFactory(pin=pin)

        kiosk = KioskFactory.build(pk=1)

        jwt = kiosk.get_authentication_token()

        interface = self.interface(jwt)

        def check_interface_state():
            """
            Checks expect state if the pin is invalid.
            """
            self.assertIsNone(interface._employee, "Should be none as invalid pin.")
            self.assertIsNone(interface._employee_pk, "Should be none as invalid pin.")
            self.assertIsNone(cache.get(interface.employee_cache_key), "Should return none as invalid pin")

        with self.subTest("With invalid pin"):
            with self.subTest("With raise exception"):
                # Should raise an exception
                with pytest.raises(InvalidPin):
                    interface.login_employee(employee, 21312, True)
                check_interface_state()

            with self.subTest("Without raising exception"):
                logged_in = interface.login_employee(employee, 21312, False)
                self.assertFalse(logged_in, "Should return false as the pin is invalid.")
                check_interface_state()

        with self.subTest("with valid pin"):

            logged_in = interface.login_employee(employee, pin)

            self.assertEqual(logged_in, True, "Should login the employee")
            self.assertEqual(
                cache.get(interface.employee_cache_key), employee.pk, "Should store the employee's pk in the cache."
            )

            self.assertEqual(interface._employee, employee, "Should set the employee on the instance.")
            self.assertEqual(interface._employee_pk, employee.pk, "Should set the employee_pk on the instance.")

    def test_update_cache(self):

        cache.clear()

        kiosk = KioskFactory.build(pk=1)

        interface = self.interface(kiosk.get_authentication_token())

        interface._employee_pk = 1
        with self.subTest("Cache hasn't expired"):
            interface._update_cache()

            self.assertEqual(
                cache.get(interface.employee_cache_key), interface._employee_pk, "Should set the value to employee pk"
            )

        with self.subTest("Cache should expire instantly"):
            interface.inactivity_amount = 0

            interface._update_cache()

            self.assertIsNone(
                cache.get(interface.employee_cache_key), "Should return None as the cache value should have expired."
            )

    def test_logout_employee(self):
        cache.clear()

        pin = 1234
        kiosk = KioskFactory.build(pk=1)
        interface = kiosk.cache
        interface.login_employee(EmployeeFactory(pin=pin), pin)

        interface.logout_employee()

        self.assertIsNone(interface.logged_in_employee, "Should logout the employee from the kiosk.")
        self.assertIsNone(cache.get(interface.employee_cache_key), "Should delete the employee login from cache")
        self.assertIsNone(interface._employee, "Should set the employee value to None")
        self.assertIsNone(interface._employee_pk, "Should set the employee_pk value to None")
