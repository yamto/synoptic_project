from factory import django, Faker

from kiosk.models import Kiosk


class KioskFactory(django.DjangoModelFactory):

    class Meta:
        model = Kiosk

    name = Faker('name')
