from django.conf import settings
from django.test import TestCase, RequestFactory

from kiosk.authentication import KioskAuthenticationMiddleware
from kiosk.tests.factories import KioskFactory


class TestsKioskAuthenticationMiddleware(TestCase):

    middleware = KioskAuthenticationMiddleware

    def test_in_middleware(self):
        self.assertIn('kiosk.authentication.KioskAuthenticationMiddleware', settings.MIDDLEWARE)

    def test_get_jwt(self):

        kiosk = KioskFactory()

        request_factory = RequestFactory()

        with self.subTest("Jwt in url_params"):
            request = request_factory.get(f'a-url?jwt={kiosk.get_authentication_token()}')

            json_web_token = self.middleware.get_jwt(request)

            self.assertEqual(
                json_web_token, kiosk.get_authentication_token(), "Should return the jwt for the kiosk instance."
            )

        with self.subTest("JWT in headers"):
            request = request_factory.get('')
            request.META['HTTP_JWT'] = kiosk.get_authentication_token()
            json_web_token = self.middleware.get_jwt(request)

            self.assertEqual(
                json_web_token, kiosk.get_authentication_token(), "Should return the jwt for the kiosk instance."
            )

        with self.subTest("No JWT provided"):
            request = request_factory.get('')

            json_web_token = self.middleware.get_jwt(request)

            self.assertIsNone(json_web_token, "Shouldn return none as the jwt is not provided.")

    def test_get_kiosk(self):

        kiosk = KioskFactory()
        with self.subTest("With invalid jwt"):
            returned_kiosk = self.middleware.get_kiosk('a_fake_jwt_token')
            self.assertIsNone(returned_kiosk, "Should return the kiosk instance")

        with self.subTest("With valid jwt"):
            returned_kiosk = self.middleware.get_kiosk(kiosk.get_authentication_token())
            self.assertEqual(kiosk, returned_kiosk, "Should return the kiosk instance for the jwt.")

    def test_integration(self):
        kiosk = KioskFactory()
        with self.subTest("With no jwt"):
            response = self.client.get(f'/?jwt={kiosk.get_authentication_token()}')
            self.assertEqual(response.wsgi_request.kiosk, kiosk, "Should return the related kiosk instance.")

        with self.subTest("With jwt in the url params"):
            response = self.client.get(f'/?jwt={kiosk.get_authentication_token()}')
            self.assertEqual(response.wsgi_request.kiosk, kiosk, "Should return the related kiosk instance.")

        with self.subTest("With jwt in the headers"):
            response = self.client.get("/", HTTP_JWT=kiosk.get_authentication_token())
            self.assertEqual(response.wsgi_request.kiosk, kiosk, "Should return the related kiosk instance.")
