from django.contrib import admin
from django.db import IntegrityError

from kiosk.models import Kiosk


class KioskAdmin(admin.ModelAdmin):
    def jwt(self, obj):

        try:
            return obj.get_authentication_token()
        except IntegrityError:
            pass

    readonly_fields = ('jwt',)


# Registers the kiosk model with the admin site.
admin.site.register(Kiosk, KioskAdmin)
