from django.core.exceptions import ObjectDoesNotExist

from kiosk.models import Kiosk


class KioskAuthenticationMiddleware:
    """
    Edits the request object to contain a kiosk attribute.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    @staticmethod
    def get_jwt(request):
        """
        Gets the JWT from the header or the url params
        """

        # Gets the jwt from the url parameter e.g. ?jwt=a.jwt.token
        jwt = request.GET.get('jwt')

        # Gets the jwt from the header
        if jwt is None:
            jwt = request.META.get('HTTP_JWT')

        return jwt

    @staticmethod
    def get_kiosk(jwt):
        """
        Returns the kiosk instance or none for the related jwt.
        """
        try:
            return Kiosk.objects.get_for_jwt(jwt)
        except ObjectDoesNotExist:
            return None

    def __call__(self, request):
        """
        Executes the middleware return the modified response.
        """

        json_web_token = self.get_jwt(request)

        request.kiosk = self.get_kiosk(json_web_token)

        response = self.get_response(request)

        return response
