import jwt
from django.conf import settings
from django.db import models, IntegrityError
from jwt import DecodeError

from employee.models import Employee
from kiosk.interfaces import KioskCacheInterface


class KioskManager(models.Manager):
    def get_for_jwt(self, json_web_token):
        """
        Returns the relevant Kiosk instance for the provided JWToken
        """
        try:
            decoded = jwt.decode(json_web_token, settings.SECRET_KEY, algorithms=['HS256'])
        except DecodeError:
            decoded = {}
        return self.get(pk=decoded.get('pk'))


class Kiosk(models.Model):
    """
    Handles interacts between the application and the database via the ORM
    """

    objects = KioskManager()

    name = models.CharField(max_length=120)

    def __init__(self, *args, **kwargs):
        super(Kiosk, self).__init__(*args, **kwargs)
        self._cache_interface = None

    @property
    def cache(self):
        """
        Returns a KioskCacheInterface for the instance.
        """
        if not self._cache_interface:
            self._cache_interface = KioskCacheInterface(self.get_authentication_token())
        return self._cache_interface

    def __str__(self):
        """
        Returns the String value of the model (the kiosk's human name)
        """
        return self.name

    def get_authentication_token(self):
        """
        Should return a JWToken to authenticate the kiosk against.
        :return: A JWToken associated with the kiosk.
        """

        if self.pk is None:
            raise IntegrityError("Kiosk has not been saved yet.")

        return jwt.encode({"pk": self.pk}, settings.SECRET_KEY, algorithm='HS256').decode("utf-8")

    @property
    def logged_in_employee(self):
        """
        Returns the logged on employee on kiosk.
        """
        return self.cache.logged_in_employee
