from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist

from employee.models import Employee


class KioskCacheInterface:
    """
    Handles cache data about each kiosk.
    """

    # How long in seconds before the cache expires
    inactivity_amount = 300

    def __init__(self, jwt):
        """
        Initialises an instance of the class.
        :param jwt: A JSON web token relating to kiosk
        """
        self.jwt = jwt
        self._employee = None
        self._employee_pk = None

    @property
    def employee_cache_key(self):
        """
        Returns the Employee cache key for the instance.
        """
        return f'{self.jwt}-logged_in'

    @property
    def logged_in_employee(self):
        """
        Returns the logged employee related to the kiosk.
        """

        if not self._employee:

            self._employee_pk = cache.get(self.employee_cache_key, None)

            try:
                self._employee = Employee.objects.get(pk=self._employee_pk)
                # Resets the inactivity time by renewing the cache value
                self._update_cache()
            except ObjectDoesNotExist:
                self._employee = None

        return self._employee

    def _login_employee(self, employee):
        """
        Actually logs the employee in to the kiosk (avoids pin checks).
        """
        self._employee = employee
        self._employee_pk = employee.pk
        self._update_cache()

    def login_employee(self, employee, pin, raise_exception=True):
        """
        Logs the Employee into the kiosk if the pin is correct.
        """

        valid = employee.is_pin_valid(pin, raise_exception)

        if valid:
            # We login the employee into the kiosk.
            self._login_employee(employee)

        return valid

    def _update_cache(self):
        """
        Inserts the employee's pk into cache with the cache key.
        """

        cache.set(self.employee_cache_key, self._employee_pk, self.inactivity_amount)

    def logout_employee(self):
        """
        Should delete the logged in from the cache.
        """

        cache.delete(self.employee_cache_key)

        self._employee = None
        self._employee_pk = None
